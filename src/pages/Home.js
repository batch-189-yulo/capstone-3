import { Row, Col } from 'react-bootstrap';
import Banner from '../components/Banner';
import Feature from '../components/Feature';
import Products from './Products';

export default function Home() {
	return(
			<>
				<Row>
					<Col className="image-bg">
						<Banner />
						<Feature />
					</Col>
				</Row>
				
			</>
		)
}