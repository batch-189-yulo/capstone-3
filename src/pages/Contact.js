import { useNavigate } from 'react-router-dom';
import { useState } from 'react';
import { Row, Col, Form, Button } from 'react-bootstrap';
import { Container } from 'react-bootstrap';
import Swal from 'sweetalert2';


export default function Contact() {

	const navigate = useNavigate()
	const [ email, setEmail ] = useState('');
	const [ firstName, setFirstName ] = useState('');
	const [ lastName, setLastName ] = useState('');
	const [ message, setMessage ] = useState('');

	function contactUs(e){
		e.preventDefault()

		fetch(`https://murmuring-sands-57849.herokuapp.com/contact/contactMessage`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				message: message
			})
		})
		.then(res => res.json())
		.then(data => {
		
		if(data){
			Swal.fire({
					title: 'Message Sent',
					icon: 'success',
					text: 'You Have Successfully Contacted Us'
				})
		setEmail('')
		setFirstName('')
		setLastName('')
		setMessage('')

			navigate('/login')
		} else {
			Swal.fire({
					title: 'Error Sending a Message',
					icon: 'error',
					text: 'Please try again'
				})
					setEmail('')
					setFirstName('')
					setLastName('')
					setMessage('')
			}
		 })



		}
		
		return(

		<Form onSubmit={e => contactUs(e)}  className="image-bg">
		<Container>
			<Row className="justify-content-center viewheight">
				<Col md={8} className="p-5">
					<h4 className="text-dark"><strong>Contact Us</strong></h4>
					<Row className="whitebg mt-4 justify-content-center p-3">
						<Col md={6}>
							<Form.Group className="mb-2">
								<Form.Label>Email Address</Form.Label>
								<Form.Control 
									size="lg"
									type="email"
									placeholder="name@example.com"
									required
									value={email}
									onChange={e => setEmail(e.target.value)}/>
							</Form.Group>

							<Form.Group className="mb-2">
							<Form.Label>Name</Form.Label>
								<Row >
								<Col md={6}>
								<Form.Control 
									size="md"
									type="string"
									placeholder="First"
									required
									value={firstName}
									onChange={e => setFirstName(e.target.value)}/>
								</Col>
								<Col md={6}>
								<Form.Control 
									size="md"
									type="string"
									placeholder="Last"
									required
									value={lastName}
									onChange={e => setLastName(e.target.value)}/>
								</Col>
								</Row>
							</Form.Group>
							
							  <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
						       <Form.Label>Comment or Message</Form.Label>
						       <Form.Control 
						       		size="lg"
						       		as="textarea" 
						       		rows={5}
						       		placeholder="How can we help you?"
									required
									value={message}
									onChange={e => setMessage(e.target.value)}/>
						     </Form.Group>
						     <Form.Check
						     type='checkbox'
						     label=<p className="text-black-50">{`I have read the Privacy Policy note. I agree that my details and data will be electronically collected and saved to answer my request. Note: You can revoke your consent at any time for the future by emailing admin@kicksph.com`}  </p>
						     />
						     	{email && firstName && lastName && message ?
									<Button variant="primary" type="submit">Submit</Button>
									:
									<Button variant="secondary" type="submit" disabled>Submit</Button>
								}
						</Col>
					</Row>
				</Col>
			</Row>
		</Container>
		</Form>

			)
		
	}




















