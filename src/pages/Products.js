import UserContext from '../UserContext';
import AdminView from '../components/AdminView';
import UserView from '../components/UserView';
import { Row, Col} from 'react-bootstrap';
import { useContext, useEffect, useState } from 'react';


export default function Products() {

	const [ allProducts, setAllProducts ] = useState([])

	const fetchData = () => {
		fetch('https://murmuring-sands-57849.herokuapp.com/products/all')
		.then(res => res.json())
		.then(data => {
			setAllProducts(data)
		})
	}
	useEffect(() => {
		fetchData()
	}, [])

	const { user } = useContext(UserContext);
	return(
		<>
			<Row className="image-bg">
				<Col>
			{(user.isAdmin === true) ?
				<AdminView productData={allProducts} fetchData={fetchData}/>
				:
				<UserView  productData={allProducts}/>
			}
				</Col>
			</Row>
		</>

		)
}