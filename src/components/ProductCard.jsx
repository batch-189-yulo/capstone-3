import { useContext } from 'react';
import { Card, Button, Col, Row, Tooltip, OverlayTrigger } from 'react-bootstrap';
import PropTypes from 'prop-types';
import UserContext from '../UserContext';
import { Link } from 'react-router-dom';



export default function ProductCard({productProp}) {
	const { _id, productName, image, price } = productProp;

	const { user } = useContext(UserContext);

	return(
		<Col xs={6} md={3} className="mb-4" >
		<Card>
			<Card.Img
				src= {`https://murmuring-sands-57849.herokuapp.com/${ image }`}
			/>
			<Card.Body className="pb-0">
				<Row>
					<Col>
						<h6 className="mb-0"> { productName } </h6>
						<h4 className="text-primary">₱{ price }</h4>
					</Col>
				</Row>
			</Card.Body>
			{(user.isAdmin === true) ?

				<Button variant="dark" className="text-light">Disabled</Button>
				:
				<OverlayTrigger placement={"top"} overlay={
      				<Tooltip>{productName}</Tooltip>}>
				<Button variant="dark" className="text-light footer-bg" as={ Link } to={`/products/${_id}`}>Check</Button>
				</OverlayTrigger>
			}
		</Card>
		</Col>
		)

} 

ProductCard.propTypes = {
	courseProp: PropTypes.shape({
		name: PropTypes.string.isRequired,
		picture: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}