import { useContext } from 'react';
import { Navbar, Nav, Col, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';
import NavDropdown from 'react-bootstrap/NavDropdown';


export default function AppNavbar() {
	const { user } = useContext(UserContext);


	return(


		<Navbar className="font-link text-light" expand="lg sm" variant="dark" sticky="top" id="navBar" >
			<Navbar.Toggle aria-controls="basic-navbar-nav"/>
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="me-auto">
				
					<Nav.Link as={Link} to="/">Home</Nav.Link>
					<Nav.Link as={Link} to="/products">Products</Nav.Link>

				<NavDropdown title="More" id="collasible-nav-dropdown">
	              <NavDropdown.Item href="#">News</NavDropdown.Item>
	              <NavDropdown.Item href="#">About Us</NavDropdown.Item>
	              <NavDropdown.Item href="#">Privacy Policy</NavDropdown.Item>
	             
	            </NavDropdown>
				</Nav>
				<Nav className="ms-auto">

				
					{(user.accessToken === null) ?
						<>
							<Nav.Link as={Link} to ="/contact">Contact Us</Nav.Link>
							<Nav.Link as={Link} to="/register">Sign Up</Nav.Link>
							<Nav.Link as={Link} to="/login">Login</Nav.Link>

						</>
						:
						<>
							<Nav.Link as={Link} to ="/contact">Contact Us</Nav.Link>
							<Nav.Link as={Link} to ="/logout">Logout</Nav.Link>


						</>
					}

					
				</Nav>
			</Navbar.Collapse>
		</Navbar>
		)
}