import { useState, useEffect } from 'react';
import { Row, Col, Container } from 'react-bootstrap';
import ProductCard from './ProductCard'

export default function UserView({productData}) {

	const [products, setProducts] = useState([])

	useEffect(() => {
		const productsArr = productData.map(product => {
			if(product.isActive === true) {
				return(
					<ProductCard key={product._id} productProp={product} />
					)

			}else{
				return null;
			}
			
		})

		setProducts(productsArr)

	}, [productData])

	return(
		<>
			
		<Container>
		<Row className="mt-3">
			<Col>
				<h3><strong>Products</strong></h3>
				<Row>
					<Col md={2}  >
						<p>Kicks aren’t just our business, they’re our lifestyle. We understand that what you wear tells a story about who you are, where you’ve been, and even where you want to go. We know this because we’ve lived it. We’ve watched this community grow with us, and we’ve been a part of it since the beginning.</p>  
					</Col>
					<Col md={10} >
						<Row>
							{ products }
						</Row>
					</Col>
				</Row>
			</Col>
		</Row>
		</Container>
		</>


		)
}

