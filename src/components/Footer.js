import { Row, Col, Container, Tooltip, OverlayTrigger, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';


export default function Footer() {
	return(
		<Row className="whitebg border justify-content-center pt-2 footer-bg">
			<Col className="text-center">

      			<OverlayTrigger placement={"top"} overlay={
      				<Tooltip>Facebook</Tooltip>}>
		        	<Button href="https://www.facebook.com/" variant="white" target="_blank">	
						<i className="bi bi-facebook text-secondary" style={{ fontSize: 25 }  }></i>
					</Button>
				</OverlayTrigger>

				<OverlayTrigger placement={"top"} overlay={
      				<Tooltip>Instagram</Tooltip>}>
					<Button href="https://www.instagram.com/" variant="white" target="_blank">	
						<i className="bi bi-instagram text-secondary" style={{ fontSize: 25 }  }></i>
					</Button>
				</OverlayTrigger>

				<OverlayTrigger placement={"top"} overlay={
      				<Tooltip>Twitter</Tooltip>}>
					<Button href="https://twitter.com/" variant="white" target="_blank">	
						<i className="bi bi-twitter text-secondary" style={{ fontSize: 25 }  }></i>
					</Button>
				</OverlayTrigger>

				<OverlayTrigger placement={"top"} overlay={
      				<Tooltip>Mail</Tooltip>}>
					<Button href="https://mail.google.com/" variant="white" target="_blank">	
						<i className="bi bi-envelope text-secondary" style={{ fontSize: 25 }  }></i>
					</Button>
				</OverlayTrigger>

				<OverlayTrigger placement={"top"} overlay={
      				<Tooltip>Youtube</Tooltip>}>
					<Button href="https://www.youtube.com/" variant="white" target="_blank">	
						<i className="bi bi-youtube text-secondary" style={{ fontSize: 25 }  }></i>
					</Button>
				</OverlayTrigger>
										
			</Col>
				<Container className="pt-2">
					<Row>
						<Col className="text-center text-secondary">
							<p>Copyright ©  2022 KicksPH®. All rights reserved.</p>
						</Col>
					</Row>
				</Container>
		</Row>
		)
}

