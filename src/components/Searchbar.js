import { useState, React } from 'react';
import { Nav } from 'react-bootstrap';
import { Row, Col, Form } from 'react-bootstrap';
import { InputGroup, Button, FormControl } from 'react-bootstrap';
import { Link } from 'react-router-dom';


export default function SearchBar() {

	const [ search, setSearch ] = useState('');
	// const [ productName, setProductName] = useState('');
	// const [ productPrice, setProductPrice] = useState('');

		
	function productSearch(e){
		e.preventDefault()
	}

	return(

		<Row className="footer-bg justify-content-center mb-2" > 
			<Col md={1} sm={4} xs={4} className="justify-content-center">
				<Nav.Link as={Link} to="/"><img className="image" src="https://i.ibb.co/RhC0kMj/Kicks-PH-logos-transparent.png" alt="Error"/>
				</Nav.Link>
			</Col>
			<Col xs={8} md={6}>
					<Form  onSubmit={e => productSearch(e)}>
					<InputGroup className="pt-3 border-secondary">
						   <Button variant="outline-light link" className="text-secondary" as={ Link } to={`/products/active/${search}`}id="button-addon1" type="submit">
						    <i className="bi bi-search"></i>
						   </Button>
						   <FormControl
						   	className="text-secondary"
						     aria-label="Example text with button addon"
						     aria-describedby="basic-addon1"
						     type="text"
						     placeholder="What are you looking for?"
						     value={search}
							 onChange={e => setSearch(e.target.value)}
						   />
					</InputGroup>
					</Form>
			</Col>
			<Col xs={2} md={1}>
				<Button variant="link" as={ Link } to={"/cart"}>	
					<i className="bi bi-cart4 text-secondary pt-5 mt-3" style={{ fontSize: 25 }  }></i>
				</Button>
			</Col>
		</Row>
		)
}


