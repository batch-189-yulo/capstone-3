import { Button, Row, Col } from 'react-bootstrap';
import Carousel from 'react-bootstrap/Carousel';
import { Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';
export default function Banner() {
	return(
		<Row className="darkbg mx-auto mt-4">
			<Col>
				<Container>
					<Row>
						<Col>
							<Carousel>
								<Carousel.Item>
									<img
									      className="d-block w-100"
									      src="https://i.ibb.co/82dh6xt/Air-Jordan-1-Category-Banner-jpg.webp"
									      alt="First slide"
									    />
									    
								</Carousel.Item>
								<Carousel.Item>
									<img
									      className="d-block w-100"
									      src="https://i.ibb.co/SKQ7V3B/Nike-SB-Dunk-Category-Banner-jpg.webp"
									      alt="First slide"
									    />
									   
								</Carousel.Item><Carousel.Item>
									<img
									      className="d-block w-100"
									      src="https://i.ibb.co/qWHkvPH/Air-Jordan-Category-Banner-jpg.webp"
									      alt="First slide"
									    />
									    
								</Carousel.Item>
							</Carousel>
						</Col>
					</Row>
				</Container>
			</Col>
		</Row>
		)
}